# UDOO Starter Kit

## Overview
A collection of useful scripts to do a handful of common tasks when setting up a new UDOO. These scripts have been run on "UDOObuntu 2.1.4 (Ubuntu 14.04 LTS trusty)".

## Scripts
- Bootstrap
- Install Node 8.x
- Install Python 3.6.x
- Install Phidget Libraries
- Other Misc. Scripts

## Create A Bootable MicroSD on Mac OS X

Go to https://www.udoo.org/downloads/ and download the right image for your board. (eg - Raspbian)

```bash
df -h
```

Unmount your SD Card using the following command:

```bash
sudo diskutil unmount /dev/[sd_name]
```

**NOTE:** Use the raw device name for using the entire disk, by omitting the final `s1` and replacing `disk` with `rdisk` (eg - `/dev/disk4s1` becomes `/dev/rdisk4`)

Start copying your Image file to the SD Card:

```bash
sudo dd bs=1M if=[img_file_path] of=/dev/[sd_name]
```

**WARNING:** Please be careful to select the correct device identifier; if you use the wrong identifier you may lose all data in your Mac!

Wait a long time for the process to finish. Then Unmount the SD Card with:

```bash
sudo diskutil eject /dev/[sd_name]
```

Done! Insert your newly created MicroSD Card in your UDOO and power it up!

## Terminal Access
The below username and password are the default upon first booting up.

- **Username:** udooer
- **Password:** udooer


## Clone microSD
On a Mac:
```
dd if=/dev/rdiskx of=/path/to/image bs=1m
```

## Set Static IP
Edit `/etc/network/interfaces`:

```
auto eth0
iface eth0 inet static
address 192.168.0.101
netmask 255.255.255.0
gateway 192.168.0.1
```
