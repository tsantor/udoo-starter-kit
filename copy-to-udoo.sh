declare -a arr=("udooer@192.168.0.102")

for i in "${arr[@]}"; do
    echo "$i"
    rsync -avzr udoo-*.sh ${i}:~

    # Run the script on the machine
    #ssh -t ${i} "./udoo-boostrap.sh"
done
