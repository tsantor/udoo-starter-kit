#!/usr/bin/env bash

./udoo-bootstrap.sh
./udoo-install-node.sh
./udoo-install-python3.sh
./udoo-install-virtualenv.sh
