#!/usr/bin/env bash

# Install NVM
sudo apt-get install build-essential libssl-dev
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | sh
source ~/.profile

# nvm install 5.12.0
# nvm use 5.12.0
# nvm alias default 5.12.0
