#!/usr/bin/env bash

PYTHON_VERSION=3.6.4

# Update and install some dependencies
sudo apt-get update
sudo apt-get -y install build-essential checkinstall
sudo apt-get -y install python3-dev libffi-dev libssl-dev

# Download, extract & build
cd ~
wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
tar xzf Python-${PYTHON_VERSION}.tgz
cd Python-${PYTHON_VERSION}
sudo ./configure  # --enable-unicode=ucs4
sudo make altinstall

# Cleanup
cd ~
sudo rm Python-${PYTHON_VERSION}.tgz
sudo rm -rf Python-${PYTHON_VERSION}
