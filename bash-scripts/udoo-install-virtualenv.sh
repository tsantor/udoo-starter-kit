#!/usr/bin/env bash

# Configure virtualenv & virtualenvwrapper
sudo apt-get -y install python-pip python3-pip
pip install virtualenv virtualenvwrapper --user

echo -e "\n# virtualenv and virtualenvwrapper" >> ~/.profile
echo "PATH=\$PATH:~/.local/bin" >> ~/.profile
echo "export WORKON_HOME=\$HOME/.virtualenvs" >> ~/.profile
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python" >> ~/.profile
echo "export PIP_DOWNLOAD_CACHE=\$HOME/.pip/cache" >> ~/.profile
# Can't find the below? find / -name virtualenvwrapper.sh
echo "source \$HOME/.local/bin/virtualenvwrapper.sh" >> ~/.profile

source ~/.profile
